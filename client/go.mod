module client

go 1.16

require (
	github.com/hidez8891/shm v0.0.0-20200313135933-0ec4df5f28c7
	github.com/r3labs/sse/v2 v2.3.3
	github.com/rs/zerolog v1.20.0
	github.com/toonknapen/accbroadcastingsdk/v3 v3.0.4
	golang.org/x/text v0.3.0
)
