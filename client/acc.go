package client

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/toonknapen/accbroadcastingsdk/v3/network"
	"time"
)

type Publisher func(interface{})

type Client struct {
	acc network.Client

	address        string
	displayName    string
	password       string
	updateInterval int32
	timeout        int32

	connected bool

	refresh         *time.Ticker
	refreshInterval time.Duration

	publisher Publisher
	log       zerolog.Logger
}

func (c *Client) startTicker() {
	c.refresh = time.NewTicker(c.refreshInterval)
	for range c.refresh.C {
		if c.connected{
			c.refreshData()
		}
	}
}

func (c *Client) refreshData() {
	c.log.Debug().Msg("refreshing track data")

	c.acc.RequestTrackData()
	c.acc.RequestEntryList()
}

func (c *Client) onConnected(connectionId int32) {
	c.log.Info().Int32("connectionId", connectionId).Msg("connected")

	c.connected = true

	c.refreshData()
	c.refresh.Reset(c.refreshInterval)
}

func (c *Client) onDisconnected() {
	c.log.Info().Msg("disconnected")

	c.connected = false
	c.refresh.Stop()
}

func (c *Client) onRealTimeUpdate(update network.RealTimeUpdate) {
	c.log.Debug().Interface("RealTimeUpdate", update).Send()

	c.publisher(update)
}

func (c *Client) onRealTimeCarUpdate(carUpdate network.RealTimeCarUpdate) {
	c.log.Debug().Interface("RealTimeCarUpdate", carUpdate).Send()

	c.publisher(carUpdate)
}

func (c *Client) onEntryList(entryList network.EntryList) {
	c.log.Debug().Interface("EntryList", entryList).Send()

	c.publisher(entryList)
}

func (c *Client) onEntryListCar(entryListCar network.EntryListCar) {
	c.log.Debug().Interface("EntryListCar", entryListCar).Send()

	c.publisher(entryListCar)
}

func (c *Client) onTrackData(trackData network.TrackData) {
	c.log.Debug().Interface("TrackData", trackData).Send()

	c.publisher(trackData)
}

func (c *Client) onBroadcastEvent(broadcastEvent network.BroadCastEvent) {
	c.log.Debug().Interface("BroadCastEvent", broadcastEvent).Send()

	c.publisher(broadcastEvent)
}

func NewClient(address, displayName, password string, updateInterval, timeout, refreshInterval time.Duration, publisher Publisher) *Client {
	client := Client{
		address:         address,
		displayName:     displayName,
		password:        password,
		updateInterval:  int32(updateInterval.Milliseconds()),
		timeout:         int32(timeout.Milliseconds()),
		refreshInterval: refreshInterval,
		publisher:       publisher,
		log:             log.With().Str("component", "ACCClient").Logger(),
	}

	subLogger := log.With().Str("component", "ACCBroadcastingSDK").Logger()

	client.acc = network.Client{
		Logger:              subLogger,
		OnConnected:         client.onConnected,
		OnDisconnected:      client.onDisconnected,
		OnRealTimeUpdate:    client.onRealTimeUpdate,
		OnRealTimeCarUpdate: client.onRealTimeCarUpdate,
		OnEntryList:         client.onEntryList,
		OnEntryListCar:      client.onEntryListCar,
		OnTrackData:         client.onTrackData,
		OnBroadCastEvent:    client.onBroadcastEvent,
	}

	go client.startTicker()

	return &client
}

func (c *Client) RunForever() {
	for {
		c.acc.ConnectListenAndCallback(c.address, c.displayName, c.password, c.updateInterval, "", c.timeout)
		if c.connected {
			c.onDisconnected()
		}
		time.Sleep(1 * time.Second)
	}
}
