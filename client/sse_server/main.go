package main

import (
	"client"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"net/http"
	"os"
	"time"

	"github.com/r3labs/sse/v2"
)

var (
	listen          = flag.String("listen", ":8080", "local server listen address")
	address         = flag.String("address", "127.0.0.1:9000", "ACC UDP address")
	displayName     = flag.String("displayname", "pitwall", "Client display name")
	password        = flag.String("password", "asd", "ACC password")
	updateInterval  = flag.Duration("update", 250*time.Millisecond, "interval for car and track updates")
	timeout         = flag.Duration("timeout", 5*time.Second, "ACC connection timeout")
	refreshInterval = flag.Duration("refresh", 1*time.Minute, "interval for entry list refreshes")
	logLevel        = flag.String("level", "info", "log level")
	jsonLog         = flag.Bool("json", false, "format log output as json")
)

func main() {
	flag.Parse()

	if !*jsonLog {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.RFC3339})
	}

	level, err := zerolog.ParseLevel(*logLevel)
	if err != nil {
		log.Fatal().Msgf("failed to parse log level: %s", err)
	}

	zerolog.SetGlobalLevel(level)

	server := newServer()

	acc := client.NewClient(*address, *displayName, *password, *updateInterval, *timeout, *refreshInterval, server.Publish)

	mux := http.NewServeMux()
	mux.Handle("/", http.FileServer(http.Dir("./web")))
	mux.HandleFunc("/events", server.server.HTTPHandler)

	go acc.RunForever()

	log.Info().Msgf("listening on %s", *listen)
	err = http.ListenAndServe(*listen, mux)
	if err != nil {
		log.Fatal().Err(err).Msgf("failed to start SSE server")
	}
}

type sseServer struct {
	server *sse.Server
}

func newServer() *sseServer {
	server := sse.New()
	server.AutoReplay = false
	server.AutoStream = true

	return &sseServer{
		server: server,
	}
}

func (s *sseServer) Publish(event interface{}) {
	raw, err := json.Marshal(event)
	if err != nil {
		panic(err)
	}

	eventType := fmt.Sprintf("%T", event)

	sseEvent := &sse.Event{
		Event: []byte(eventType),
		Data:  raw,
	}

	s.server.Publish("all", sseEvent)
	s.server.Publish(eventType, sseEvent)
}
