package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"golang.org/x/text/encoding/unicode"
	"reflect"
	"time"

	"github.com/hidez8891/shm"
)

type SPageFilePhysics struct {
	PacketId           int32
	Gas                float32
	Brake              float32
	Gear               int32
	RPM                int32
	SteerAngle         float32
	SpeedKMH           float32
	Velocity           [3]float32
	AccG               [3]float32
	WheelSlip          [4]float32
	WheelLoad          [4]float32
	WheelPressure      [4]float32
	WheelAngularSpeed  [4]float32
	TyreWear           [4]float32
	TyreDirtyLevel     [4]float32
	TyreCoreTemp       [4]float32
	CamberRAD          [4]float32
	SuspensionTravel   [4]float32
	DRS                float32
	TC                 float32
	Heading            float32
	Pitch              float32
	Roll               float32
	CGHeight           float32
	CarDamage          [5]float32
	NumberOfTyresOut   int32
	PitLimiterOn       int32
	ABS                float32
	KERSCharge         float32
	KERSInput          float32
	AutoShifterOn      int32
	RideHeight         [2]float32
	TurboBoost         float32
	Ballast            float32
	AirDensity         float32
	AirTemp            float32
	RoadTemp           float32
	LocalAngularVel    [3]float32
	FinalFF            float32
	PerformanceMeter   float32
	EngineBrake        int32
	ERSRecoveryLevel   int32
	ERSPowerLevel      int32
	ERSHeatCharging    int32
	ERSIsCharging      int32
	KERSCurrentKJ      float32
	DRSAvailable       int32
	DRSEnabled         int32
	BrakeTemp          [4]float32
	Clutch             float32
	TyreTempI          [4]float32
	TyreTempM          [4]float32
	TyreTempO          [4]float32
	IsAIControlled     int32
	TyreContactPoint   [4][3]float32
	TyreContactNormal  [4][3]float32
	TyreContactHeading [4][3]float32
	BrakeBias          float32
	LocalVelocity      [3]float32
	P2PActivation      int32
	P2PStatus          int32
	CurrentMaxRPM      float32
	MZ                 [4]float32
	Fx                 [4]float32
	Fy                 [4]float32
	SlipRatio          [4]float32
	slipAngle          [4]float32
	TCInAction         int32
	ABSInAction        int32
	SuspensionDamage   [4]float32
	TyreTemp           [4]float32
	WaterTemp          float32
	BrakePressure      [4]float32
	FrontBrakeCompound int32
	RearBrakeCompound  int32
	PadLife            [4]float32
	DiscLife           [4]float32
	IgnitionOn         int32
	StarterEngineOn    int32
	IsEngineRunning    int32
	KerbVibration      float32
	SlipVibrations     float32
	GVibrations        float32
	ABSVibrations      float32
}

func main() {
	loop()
}

func loop() {
	size := int32(reflect.TypeOf(SPageFileGraphics{}).Size())
	rbuf := make([]byte, size)
	var data SPageFileGraphics

	re, err := shm.Open("Local\\acpmf_graphics", size)
	if err != nil {
		panic(err)
	}

	for {
		_, err = re.ReadAt(rbuf, 0)
		if err != nil {
			panic(err)
		}

		if err = binary.Read(bytes.NewReader(rbuf), binary.LittleEndian, &data); err != nil {
			fmt.Println("binary.Read failed:", err)
		}
		fmt.Printf("%+v\n", data)
		fmt.Printf("CurrentTime:      %s\n", wcharString(data.CurrentTime[:]))
		fmt.Printf("LastTime:         %s\n", wcharString(data.LastTime[:]))
		fmt.Printf("BestTime:         %s\n", wcharString(data.BestTime[:]))
		fmt.Printf("Split:            %s\n", wcharString(data.Split[:]))
		fmt.Printf("TyreCompound:     %s\n", wcharString(data.TyreCompound[:]))
		fmt.Printf("DeltaLapTime:     %s\n", wcharString(data.DeltaLapTime[:]))
		fmt.Printf("EstimatedLapTime: %s\n", wcharString(data.EstimatedLapTime[:]))
		fmt.Printf("TrackStatus:      %s\n", wcharString(data.TrackStatus[:]))

		time.Sleep(1 * time.Second)
	}
}

func wcharString(b []byte) string {
	decoder := unicode.UTF16(unicode.LittleEndian, unicode.UseBOM).NewDecoder()
	out, err := decoder.Bytes(b)
	if err != nil {
		panic(err)
	}
	i := bytes.IndexByte(out, 0)
	if i == -1 {
		i = len(out)
	}
	return string(out[:i])
}

type SPageFileGraphics struct {
	PacketId                 int32
	ACCStatus                int32
	ACCSessionType           int32
	CurrentTime              [30]byte // ?
	LastTime                 [30]byte // ?
	BestTime                 [30]byte // ?
	Split                    [30]byte // ?
	CompletedLaps            int32
	Position                 int32
	ICurrentTime             int32
	ILastTime                int32
	IBestTime                int32
	SessionTimeLeft          float32
	DistanceTraveled         float32
	IsInPit                  int32
	CurrentSectorIndex       int32
	LastSectorTime           int32
	NumberOfLaps             int32
	TyreCompound             [68]byte // ?
	ReplayTimeMultiplier     float32
	NormalizedCarPosition    float32
	ActiveCars               int32
	CarCoordinates           [60][3]float32
	CarId                    [60]int32
	PlayerCarId              int32
	PenaltyTime              float32
	Flag                     int32
	PenaltyType              int32
	IsIdealLineOn            int32
	IsInPitLane              int32
	SurfaceGrip              float32
	MandatoryPitDone         int32
	WindSpeed                float32
	WindDirection            float32
	IsSetupMenuVisible       int32
	MainDisplayIndex         int32
	SecondaryDisplayIndex    int32
	TC                       int32
	TCCut                    int32
	EngineMap                int32
	ABS                      int32
	FuelXLap                 int32
	RainLights               int32
	FlashingLights           int32
	LightsStage              int32
	ExhaustTemperature       float32
	WiperLV                  int32
	DriverStintTotalTimeLeft int32
	DriverStintTimeLeft      int32
	RainTyres                int32
	SessionIndex             int32
	UsedFuel                 float32
	DeltaLapTime             [32]byte // ?
	IDeltaLapTime            int32
	EstimatedLapTime         [32]byte // ?
	IEstimatedLapTime        int32
	IsDeltaPositive          int32
	ISplit                   int32
	IsValidLap               int32
	FuelEstimatedLaps        float32
	TrackStatus              [68]byte // ?
	MissingMandatoryPits     int32
	Clock                    float32
	DirectionLightsLeft      int32
	DirectionLightsRight     int32
	GlobalYellow             int32
	GlobalYellow1            int32
	GlobalYellow2            int32
	GlobalYellow3            int32
	GlobalWhite              int32
	GlobalGreen              int32
	GlobalChequered          int32
	GlobalRed                int32
	MFDTyreSet               int32
	MFDFuelToAdd             float32
	MFDTyrePressureLF        float32
	MFDTyrePressureRF        float32
	MFDTyrePressureLR        float32
	MFDTyrePressureRR        float32
	TrackGripStatus          int32
	RainIntensity            int32
	RainIntensityIn10Min     int32
	RainIntensityIn30Min     int32
	CurrentTyreSet           int32
	StrategyTyreSet          int32
}
