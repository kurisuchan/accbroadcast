import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const invalidTime = 2147483647
const dummyLap = {
  LapTimeMs: invalidTime,
  CarId: 0,
  DriverId: 0,
  Splits: [0, 0, 0],
  IsInvalid: 1,
  IsValidForBest: 0,
  IsOutLap: 0,
  IsInLap: 0
}
const dummyCar = {
  Id: 0,
  DriverId: 0,
  DriverCount: 1,
  Gear: 0,
  WorldPosX: 0.0,
  WorldPosY: 0.0,
  Yaw: 0.0,
  CarLocation: 1,
  Kmh: 0,
  Position: -1,
  CupPosition: -1,
  TrackPosition: -1,
  SplinePosition: 0.0,
  Laps: 0,
  Delta: 0,
  BestSessionLap: dummyLap,
  LastLap: dummyLap,
  CurrentLap: dummyLap
}
const dummyEntry = {
  Id: 0,
  Model: -1,
  TeamName: '',
  RaceNumber: -1,
  CupCategory: -1,
  CurrentDriverId: 0,
  Nationality: 0,
  Drivers: [{
    FirstName: '',
    LastName: '',
    ShortName: '',
    Category: 0,
    Nationality: 0
  }]
}

export default new Vuex.Store({
  state: {
    messages: [],
    cars: {
      0: {
        car: dummyCar,
        entry: dummyEntry
      }
    },
    broadcast: {
      Type: 5,
      Msg: '--:--.---',
      TimeMs: 0,
      CarId: 0
    },
    track: {
      Id: 0,
      Name: '',
      Meters: 0
    },
    session: {
      EventIndex: 0,
      SessionIndex: 0,
      SessionType: 0,
      Phase: 0,
      SessionTime: 0,
      SessionEndTime: 0,
      FocusedCarIndex: 0,
      ActiveCameraSet: '',
      CurrentHUDPage: '',
      IsReplayPlaying: 0,
      TimeOfDay: 0.0,
      AmbientTemp: 0,
      TrackTemp: 0,
      Clouds: 0,
      RainLevel: 0,
      Wettness: 0, /* [sic] */
      BestSessionLap: dummyLap
    }
  },
  mutations: {
    updateCar (state, car) {
      if (state.cars[car.Id] === undefined) {
        Vue.set(state.cars, car.Id, {
          car: car,
          entry: dummyEntry
        })
      } else {
        Vue.set(state.cars[car.Id], 'car', car)
      }
    },
    updateSession (state, session) {
      state.session = session
    },
    updateEntryList (state, entryList) {
      Object.keys(state.cars).filter((id) => !entryList.map(String).includes(id))
        .forEach((id) => {
          Vue.delete(state.cars, id)
        })
    },
    updateEntryListCar (state, entryListCar) {
      if (state.cars[entryListCar.Id] === undefined) {
        Vue.set(state.cars, entryListCar.Id, {
          car: dummyCar,
          entry: entryListCar
        })
      } else {
        Vue.set(state.cars[entryListCar.Id], 'entry', entryListCar)
      }
    },
    updateBroadcast (state, broadcast) {
      console.log('BroadCast', broadcast)
      state.broadcast = broadcast
      state.messages.push(broadcast)
    },
    updateTrackData (state, trackData) {
      state.track = trackData
    }
  },
  getters: {
    sortedCars (state) {
      return Object.values(state.cars).sort((a, b) => a.car.Position - b.car.Position)
    }
  },
  actions: {},
  modules: {}
})
