import Vue from 'vue'
import App from './App.vue'
import store from './store'
import stateStuff from './mixins/stateStuff'
import VueSSE from 'vue-sse'

Vue.config.productionTip = false

Vue.use(VueSSE)
Vue.mixin(stateStuff)

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
