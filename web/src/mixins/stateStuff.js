export default {
  methods: {
    driver (car) {
      return car.entry.Drivers[car.car.DriverId]
    },
    driverFullName (car) {
      const driver = this.driver(car)

      return driver === undefined ? '' : `${driver.FirstName} ${driver.LastName}`
    },
    driverInitials (car) {
      const driver = this.driver(car)

      return driver === undefined ? '' : driver.ShortName
    },
    driverCategory (car) {
      const driver = this.driver(car)

      switch (driver?.Category) {
        case 3:
          return 'Platinum'
        case 2:
          return 'Gold'
        case 1:
          return 'Silver'
        case 0:
          return 'Bronze'
        case 255:
          return 'Error'
        default:
          return `Unknown(${driver?.Category})`
      }
    },
    cupCategory (car) {
      const category = car.entry.CupCategory

      switch (category) {
        case 0:
          return 'Pro'
        case 1:
          return 'Pro-Am'
        case 2:
          return 'Am'
        case 3:
          return 'Silver'
        case 4:
          return 'National'
        default:
          return `Unknown(${category})`
      }
    },
    carModel (car) {
      const model = car.entry.Model

      switch (model) {
        case 0:
          return 'Porsche 991 GT3'
        case 1:
          return 'Mercedes AMG GT3'
        case 2:
          return 'Ferrari 488 GT3'
        case 3:
          return 'Audi R8 GT3 2015'
        case 4:
          return 'Lamborghini Huracan GT3'
        case 5:
          return 'McLaren 650s GT3'
        case 6:
          return 'Nissan GT-R Nismo GT3 2018'
        case 7:
          return 'BMW M6 GT3'
        case 8:
          return 'Bentley Continental GT3 2018'
        case 9:
          return 'Porsche 991 II GT3 Cup'
        case 10:
          return 'Nissan GT-R Nismo GT3 2015'
        case 11:
          return 'Bentley Continental GT3 2015'
        case 12:
          return 'Aston Martin Vantage V12 GT3'
        case 13:
          return 'Lamborghini Gallardo R-EX'
        case 14:
          return 'Emil Frey Jaguar G3'
        case 15:
          return 'Lexus RC F GT3'
        case 16:
          return 'Lamborghini Huracan Evo 2019'
        case 17:
          return 'Honda NSX GT3'
        case 18:
          return 'Lamborghini Huracan SuperTrofeo'
        case 19:
          return 'Audi R8 LMS Evo 2019'
        case 20:
          return 'Aston Martin Vantage V8 2019'
        case 21:
          return 'Honda NSX Evo 2019'
        case 22:
          return 'McLaren 720S GT3 Special'
        case 23:
          return 'Porsche 991 II GT3 R 2019'
        case 24:
          return 'Ferrari 488 GT3 Evo'
        case 25:
          return 'Mercedes AMG GT3 2020'
        case 50:
          return 'Alpine A110 GT4'
        case 51:
          return 'Aston Martin Vantage GT4'
        case 52:
          return 'Audi R8 LMS GT4'
        case 53:
          return 'BMW M4 GT4'
        case 55:
          return 'Chevrolet Camaro GT4'
        case 56:
          return 'Ginetta G55 GT4'
        case 57:
          return 'KTM X-Bow GT4'
        case 58:
          return 'Maserati MC GT4'
        case 59:
          return 'McLaren 570S GT4'
        case 60:
          return 'Mercedes AMG GT4'
        case 61:
          return 'Porsche 718 Cayman GT4'
        default:
          return `Unknown(${model})`
      }
    },
    isGT4 (car) {
      return car.entry.Model >= 50
    },
    carLocation (car) {
      switch (car.car.CarLocation) {
        case 0:
          return 'None'
        case 1:
          return 'Track'
        case 2:
          return 'PitLane'
        case 3:
          return 'PitEntry'
        case 4:
          return 'PitExit'
        default:
          return `Unknown(${car.car.CarLocation})`
      }
    },
    lapType (car, lapName) {
      const lap = car.car[lapName]

      let out = ''
      if (lap.IsOutLap) {
        out += 'O'
      }
      if (lap.IsInLap) {
        out += 'I'
      }
      if (lap.IsInvalid) {
        out += 'X'
      }
      if (lap.IsValidForBest) {
        out += 'V'
      }
      if (out === '') {
        out = '?'
      }
      return out
    },
    validClass (car, lapName) {
      const lap = car.car[lapName]

      if (lap?.IsInvalid) {
        return 'lap-invalid'
      }
      if (lap?.IsOutLap) {
        return 'lap-outlap'
      }
      if (lap?.IsValidForBest && lap?.LapTimeMs !== 2147483647) {
        return 'lap-valid'
      }
      return ''
    },
    bestClass (car, lapName, sectorIndex) {
      const time = car.car[lapName].Splits[sectorIndex]

      if (time !== 0 && Object.values(this.cars).filter((car) => car.car[lapName].IsValidForBest && !car.car[lapName].IsInvalid && car.car[lapName].Splits[sectorIndex] > 0 && car.car[lapName].Splits[sectorIndex] < time).length === 0) {
        return 'lap-best'
      } else {
        return ''
      }
    },
    sec2time (timeInSeconds) {
      if (timeInSeconds === 2147483647 / 1000) {
        return 'no time'
      }

      const pad = function (num, size) {
        return ('000' + num).slice(size * -1)
      }

      const time = parseFloat(timeInSeconds).toFixed(3)
      const hours = Math.floor(time / 60 / 60)
      const minutes = Math.floor(time / 60) % 60
      const seconds = Math.floor(time - minutes * 60)
      const milliseconds = time.slice(-3)

      return (hours !== 0 ? pad(hours, 2) + ':' : '') + pad(minutes, 2) + ':' + pad(seconds, 2) + ',' + pad(milliseconds, 3)
    },
    lapTime (car, lapName) {
      const time = car.car[lapName]?.LapTimeMs

      if (time !== undefined) {
        return this.sec2time(time / 1000)
      }
    },
    sectorTime (car, lapName, sectorIndex) {
      const time = car.car[lapName]?.Splits[sectorIndex]

      if (time !== undefined) {
        return this.sec2time(time / 1000)
      }
    },
    splinePosition (car) {
      return Math.round(car.car.SplinePosition * 100)
    },
    sessionType (session) {
      switch (session.SessionType) {
        case 0:
          return 'Practice'
        case 4:
          return 'Qualifying'
        case 9:
          return 'Superpole'
        case 10:
          return 'Race'
        case 11:
          return 'Hotlap'
        case 12:
          return 'Hotstint'
        case 13:
          return 'Hotlap Superpole'
        case 14:
          return 'Replay'
        default:
          return 'Unknown'
      }
    },
    sessionPhase (session) {
      switch (session.Phase) {
        case 0:
          return 'None'
        case 1:
          return 'Starting'
        case 2:
          return 'PreFormation'
        case 3:
          return 'FormationLap'
        case 4:
          return 'PreSession'
        case 5:
          return 'Session'
        case 6:
          return 'SessionOver'
        case 7:
          return 'PostSession'
        case 8:
          return 'ResultUI'
        default:
          return 'Unknown'
      }
    },
    broadcastType (broadcast) {
      switch (broadcast.Type) {
        case 0:
          return 'None'
        case 1:
          return 'GreenFlag' // Unused
        case 2:
          return 'SessionOver' // Unused
        case 3:
          return 'PenaltyCommMsg' // Unused
        case 4:
          return 'Accident' // Unused
        case 5:
          return 'LapCompleted'
        case 6:
          return 'BestSessionLap'
        case 7:
          return 'BestPersonalLap'
        default:
          return `Unknown(${broadcast.Type})`
      }
    }
  }
}
