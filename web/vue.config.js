module.exports = {
  devServer: {
    proxy: {
      '^/events': {
        target: 'http://localhost:8000',
        changeOrigin: true
      }
    }
  }
}
